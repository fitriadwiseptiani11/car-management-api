exports.ROLES = {
  SUPERADMIN: "superadmin",
  ADMIN: "admin",
  MEMBER: "member",
};

exports.JWT = {
  SECRET: "jwt_secret",
  EXPIRED: "24h",
};