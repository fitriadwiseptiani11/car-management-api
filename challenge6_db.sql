-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2022 at 01:37 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `challenge6_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `plate` varchar(255) DEFAULT NULL,
  `manufacture` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `rent_per_day` int(11) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `transmission` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `options` varchar(255) DEFAULT NULL,
  `specs` varchar(255) DEFAULT NULL,
  `available_at` varchar(255) DEFAULT NULL,
  `is_with_driver` tinyint(1) DEFAULT NULL,
  `created_by` varchar(255) NOT NULL,
  `updated_by` varchar(255) NOT NULL,
  `deleted_by` varchar(255) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `plate`, `manufacture`, `model`, `image`, `rent_per_day`, `capacity`, `description`, `transmission`, `type`, `year`, `options`, `specs`, `available_at`, `is_with_driver`, `created_by`, `updated_by`, `deleted_by`, `createdAt`, `updatedAt`) VALUES
(1, 'DBH-3491', 'Ford', 'F150', './images/car01.min.jpg', NULL, 2, ' Brake assist. Leather-wrapped shift knob. Glove box lamp. Air conditioning w/in-cabin microfilter.', 'Automatic', 'Sedan', '2022', 'Cruise Control', 'Leather-wrapped shift knob', NULL, NULL, '', '', '', '2022-05-12 21:32:02', '2022-05-12 21:32:02'),
(2, 'Mobil kedua updated dua', 'BMW', 'X5', './images/car02.min.jpg', 800000, 6, ' Rear passenger map pockets. Electrochromic rearview mirror. Dual chrome exhaust tips. Locking glove box.', 'Automatic', 'Convertible', '2019', 'Keyless Entry', 'Rear passenger map pockets', '2022-03-23T15:49:05.563Z', NULL, '', 'admin4', '', '2022-05-12 21:33:09', '2022-05-13 18:42:45'),
(4, 'Mobil keempat', 'BMW', 'X5', './images/car02.min.jpg', 800000, 6, ' Rear passenger map pockets. Electrochromic rearview mirror. Dual chrome exhaust tips. Locking glove box.', 'Automatic', 'Convertible', '2019', NULL, 'Rear passenger map pockets', '2022-03-23T15:49:05.563Z', 1, 'admin4', '', '', '2022-05-13 18:41:58', '2022-05-13 18:41:58'),
(5, 'Mobil kelima', 'Ferrari', 'X5', './images/car02.min.jpg', 800000, 6, ' Rear passenger map pockets. Electrochromic rearview mirror. Dual chrome exhaust tips. Locking glove box.', 'Automatic', 'Convertible', '2019', NULL, 'Rear passenger map pockets', '2022-03-23T15:49:05.563Z', 1, 'admin4', '', '', '2022-05-13 22:16:53', '2022-05-13 22:16:53');

-- --------------------------------------------------------

--
-- Table structure for table `sequelizemeta`
--

CREATE TABLE `sequelizemeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `refresh_token` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `refresh_token`, `createdAt`, `updatedAt`) VALUES
(1, 'superadmin', 'superadmin@gmail.com', '$2b$10$CEGa4ch/bpNJoqsnv3ptleKT4Bx7qPhE0GZtyDPZMH9nn3.Nahcye', 'superadmin', '', '2022-05-12 17:58:43', '2022-05-12 23:52:53'),
(2, 'superadmin', 'superadmin@gmail.com', '$2b$10$NKr7wLDeExP754wnBvn1y.lML.8HH1IyEF5ZqBkvjmq0ZsLLQK3/C', 'superadmin', '', '2022-05-12 18:19:20', '2022-05-12 18:19:20'),
(3, 'SUPERADMIN', 'superadmin@gmail.com', '$2b$10$69fLCVa2wYJYoSCF0DmVB.tQ7qXQUy.fAo8/fMIb2OBfhL4WJBkie', 'superadmin', '', '2022-05-12 20:03:52', '2022-05-12 20:03:52'),
(4, 'Member1', 'member1@gmail.com', '$2b$10$RqJqBIULU9VY3R/VQonULuhB7mbhusmn8BhrX7BFH4InamgLLtjWC', 'member', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsIm5hbWUiOiJNZW1iZXIxIiwiZW1haWwiOiJtZW1iZXIxQGdtYWlsLmNvbSIsInJvbGUiOiJtZW1iZXIiLCJpYXQiOjE2NTIzOTM1NjcsImV4cCI6MTY1MjQ3OTk2N30.uk8Qhp5hRmtbF5Pcmmwe0zUZzNpPjD4J1QnaU90Hh8I', '2022-05-12 21:02:00', '2022-05-12 22:12:47'),
(5, 'Admin1', 'admin1@gmail.com', '$2b$10$xiEidvXiSQR5P5S2E6Tv/O4muSEIJoPpzZ9VxTeK9Et/bEBf8l9MK', 'admin', '', '2022-05-12 22:12:40', '2022-05-12 22:12:40'),
(6, 'member2', 'member2@gmail.com', '$2b$10$u2L3dyzQ2mF57eEisDEuneL/UXX3ihHPdICp/sVtBFvlzc1Muy2eO', 'member', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjYsIm5hbWUiOiJtZW1iZXIyIiwiZW1haWwiOiJtZW1iZXIyQGdtYWlsLmNvbSIsInJvbGUiOiJtZW1iZXIiLCJpYXQiOjE2NTIzOTM3NTgsImV4cCI6MTY1MjQ4MDE1OH0.-iuc8oDilq5baKvEYJqo6baI8oYEbfEJVpSTKYqLVc0', '2022-05-12 22:15:40', '2022-05-12 22:15:58'),
(7, 'admin3', 'admin3@gmail.com', '$2b$10$.u/RJqCYAjxiDegSrpPjt.wLiiHHPWNShML8NFheAJPZ2ZnOK3HUa', 'admin', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjcsIm5hbWUiOiJhZG1pbjMiLCJlbWFpbCI6ImFkbWluM0BnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NTIzOTg0OTcsImV4cCI6MTY1MjQ4NDg5N30.zl7jkKczdcIWTV8KPIJRuU-UjZu0PUDvwoVVC4VBCbs', '2022-05-12 22:21:29', '2022-05-12 23:34:57'),
(8, 'superadmin', 'superadmin@gmail.com', '$2b$10$DxI/7EN7vMOmWSJtktmiGeqWg2/YKrjSJvmLwiticS6OjnW2yyI86', 'superadmin', '', '2022-05-12 23:03:46', '2022-05-12 23:03:46'),
(9, 'admin4', 'admin4@gmail.com', '$2b$10$vNyk/5TiejpJJuHRHasmiepLvJx1Q/FXmFL.AHzWVJxHYB0fxFQWO', 'admin', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjksIm5hbWUiOiJhZG1pbjQiLCJlbWFpbCI6ImFkbWluNEBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NTIzOTk1MjksImV4cCI6MTY1MjQ4NTkyOX0.Z8bRV-WchaXGOWLST22gB7mx9hsSAl4ytF9xPbMRqiw', '2022-05-12 23:51:37', '2022-05-12 23:52:09'),
(10, 'admin5', 'admin5@gmail.com', '$2b$10$Vdu4VKTfEXZPfynbKLsVv.x/pImR1sOmjTJGcnoBPxigoN.8eiYmi', 'member', '', '2022-05-13 16:50:11', '2022-05-13 16:50:11'),
(11, 'member4', 'member4@gmail.com', '$2b$10$KlEZcNGXsvisjOArFHngmeVdMANiC3YzWje2P6.qfhy./aCZWbAWS', 'member', '', '2022-05-13 17:00:11', '2022-05-13 17:00:11'),
(12, 'member5', 'member5@gmail.com', '$2b$10$9Xf9hVSqm.vYldOEoGJlneEKSfzkpj0tfuAnVe4zXPJxheCIt1omC', 'member', '', '2022-05-13 22:14:26', '2022-05-13 22:14:26'),
(13, 'superadmin2', 'superadmin2@gmail.com', '$2b$10$uuIg0U3lvP71urEWF.j9oeWu8/BRNYN9b9EQgnAjTS6N5yERP3GOC', 'superadmin', '', '2022-05-13 22:38:05', '2022-05-13 22:38:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sequelizemeta`
--
ALTER TABLE `sequelizemeta`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
