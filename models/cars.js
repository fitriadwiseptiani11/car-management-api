'use strict';
const { v4: uuidv4 } = require('uuid');
const {
  Model
} = require('sequelize');
const { options } = require('nodemon/lib/config');
module.exports = (sequelize, DataTypes) => {
  class Cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Cars.init({
    plate: DataTypes.STRING,
    manufacture: DataTypes.STRING,
    model: DataTypes.STRING,
    image: DataTypes.STRING,
    rent_per_day: DataTypes.INTEGER,
    capacity: DataTypes.INTEGER,
    description: DataTypes.STRING,
    transmission: DataTypes.STRING,
    type: DataTypes.STRING,
    year: DataTypes.STRING,
    option: DataTypes.STRING,
    specs: DataTypes.STRING,
    available_at: DataTypes.STRING,
    is_with_driver: DataTypes.BOOLEAN,
    created_by: DataTypes.STRING,
    updated_by: DataTypes.STRING,
    deleted_by: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Cars',
  });
  return Cars;
};