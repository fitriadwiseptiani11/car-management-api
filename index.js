const express = require("express");
const dotenv = require("dotenv");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const db = require("./config/Database.js");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");
// import Users from "./models/UserModel.js";  //Untuk menjalankan perintah eksekusi pembuatan database users
// import Cars from "./models/cars.js";
// const router = require("./routes/index.js");

dotenv.config();
const app = express();
app.use(express.json());

// Import Controllers
const usersController = require("./controllers/usersController");
const carsController = require("./controllers/carsController");

// Import Midleware
const middleware = require("./middleware/auth");

//Router untuk user
app.get('/users', usersController.getUsers);
app.post('/users', usersController.register);
app.post("/admin", middleware.authenticate, middleware.isSuperAdmin, usersController.register);
app.post('/login', usersController.login);
app.get('/me', middleware.authenticate, usersController.currentUser);
//Cars
app.get("/cars", middleware.authenticate, carsController.getAll);
app.post("/cars", middleware.authenticate, middleware.isAdminSuperAdmin, carsController.create);
app.put("/cars/update/:id", middleware.authenticate, middleware.isAdminSuperAdmin, carsController.updateCars);
app.delete("/cars/delete/:id", middleware.authenticate, middleware.isAdminSuperAdmin, carsController.deleteCars);

//Dokumentasi swagger
app.use("/api/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//Koneksi database
db
 .authenticate()
 .then(() => {
  console.log('Connection has been established successfully.')
 })
 .catch(err => {
  console.error('Unable to connect to the database:', err)
 })

app.use(cors({ credentials:true, origin:'htpp://localhost:3000' }));
app.use(cookieParser())
// app.use(router);  //middleware

app.listen(5000, ()=> console.log('Server running at port 5000'));