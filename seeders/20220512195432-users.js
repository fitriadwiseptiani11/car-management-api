"use strict";
const bcrypt = require("bcrypt");
const SALT_ROUND = 10;
const password = "superadmin";


module.exports = {
  async up(queryInterface, Sequelize) {
    const hashedPassword = await bcrypt.hash(password, SALT_ROUND);
    await queryInterface.bulkInsert(
      "users",
      [
        {
          name: "SUPERADMIN",
          email: "superadmin@gmail.com",
          role: "superadmin",
          password: hashedPassword,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("users", null, {});
  },
};
