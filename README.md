# Car Management API

Kelompok 6 :
Fitria Dwi Septiani
Bimo Ganang
Altheo Hardian

### Built With

- ExpressJS
- Sequelize
- MySQL
- Swagger Open API

## Info
- Swagger Car Management API

http://localhost:5000/api/docs/
```

- Super Admin Account
   email: superadmin@gmail.com
   password: superadmin
