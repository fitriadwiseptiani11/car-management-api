const authService = require("../service/usersService");
const bcrypt = require("bcrypt");
const { getUser } = require("../repository/usersRepository");

// const jwt = require("jsonwebtoken");

const getUsers = async(req, res) => {
  try {
      const users = await getUser.findAll({
          attributes:['id','name','email', 'role']
      });
      res.json(users);
  } catch (error) {
      console.log(error);
  }
};

const getUsersById = async(req, res) => {
    const { id } = req.params;
    try {
        const users = await Users.findAll({
            attributes:['id','name','email', 'role']
        });
        res.json(users);
    } catch (error) {
        console.log(error);
    }
  };

//Register user baru
const register = async(req, res) => {
    // const role = "member";
    const { name, email, password, role } = req.body;
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    const { status, status_code, message, data } = await authService.register({
        name,
        email,
        password: hashPassword,
        role,
      });
      res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
};

//Register admin atau superadmin
// const registerAdmin = async (req, res) => {
//     if (req.user.role != "superadmin") {
//       res.status(401).json({
//         status: "Unauthorized",
//         message: "You are not authorized to register an admin",
//       });
//       return;
//     }
  
//     const role = "admin";
//     const { name, email, password } = req.body;
//     const hashPassword = await bcrypt.hash(password, salt);
//     const newUser = {
//         role,
//         name,
//         email,
//         password: hashPassword,
//       };
//       res.status(status_code).send({
//         status: status,
//         message: message,
//         data: data,
//     });
//   };

//Data current User
const currentUser = async (req, res) => {
  const currentUser = req.user;

  res.status(200).send({
      status: true,
      message: "Get current user success.",
      data: {
          user: currentUser,
      },
  });
};

//Login user
const login = async(req, res) => {
    const { email, password } = req.body;

    const { status, status_code, message, data } = await authService.login({
        email,
        password,
    });

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
};

module.exports = { getUsers, getUsersById, register, currentUser, login };