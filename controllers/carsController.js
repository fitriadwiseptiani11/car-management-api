// import model product
const carsService = require("../service/carsService.js");

//Get semua product
const getAll= async(req, res) => {
  const { status, status_code, message, data } = await carsService.getAll();

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
}

//Get product berdasarkan id
// const getCarsByid = async(req, res) => {
//   try{
//     const cars = await Cars.findAll({
//       where: {
//           id: req.params.id
//       }
//     });
//     res.send(cars[0]);
//   } catch (err){
//     console.log(err);
//   }
// };

//create product baru
const create = async(req, res, next) => {
  const {
    plate,
    manufacture,
    model,
    image,
    rent_per_day,
    capacity,
    description,
    transmission,
    type,
    year,
    options,
    specs,
    available_at,
    is_with_driver,
} = req.body;
    const created_by = req.user.name;

    const { status, status_code, message, data } = await carsService.create({
      plate,
      manufacture,
      model,
      image,
      rent_per_day,
      capacity,
      description,
      transmission,
      type,
      year,
      options,
      specs,
      available_at,
      is_with_driver,
      created_by,
  });

  res.status(status_code).send({
    status: status,
    message: message,
    data: data,
  });
};

//update product
const updateCars = async(req, res) => {
  const { id } = req.params;
    const {
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
    } = req.body;
    const updated_by = req.user.name;
    const { status, status_code, message, data } = await carsService.updateCars({
      id,
      plate,
      manufacture,
      model,
      image,
      rent_per_day,
      capacity,
      description,
      transmission,
      type,
      year,
      options,
      specs,
      available_at,
      is_with_driver,
      updated_by,
  });

  res.status(status_code).send({
      status: status,
      message: message,
      data: data,
  });
};

// delete product berdasarkan id
const deleteCars = async(req, res) => {
  const { id } = req.params;

    const deleted_by = req.user.name

    const { status, code_status, message, data } = await carsService.deletedCars({
        id,
        deleted_by
    });

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
};

 const getByAvailable = async (req, res) => {
  const { available } = req.body;

  const { status, code_status, message, data } =
    await carsService.getByAvailable({ available });

  res.status(code_status).send({
    status: status,
    message: message,
    data: data,
  });
};

module.exports = { getAll, create, updateCars, deleteCars, getByAvailable };