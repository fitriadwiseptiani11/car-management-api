const { Cars } = require("../models");

class CarsRepository {
    static async create({
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        created_by,
    }) {
        const createdCar = Cars.create({
            plate,
            manufacture,
            model,
            image,
            rent_per_day,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            available_at,
            is_with_driver,
            created_by,
        });

        return createdCar;
    }

    static async getAll() {
        const carsData = await Cars.findAll();
        return carsData;
    }

    static async updateCars({
        id,
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        updated_by,
    }) {
        const updatedCar = Cars.update({
            plate,
            manufacture,
            model,
            image,
            rent_per_day,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            available_at,
            is_with_driver,
            updated_by,
        },{
            where: {
                id
            }
        });

        return updatedCar;
    }

    static async deletedCars({
        id
    }) {
        const deletedCar = await Cars.destroy({
            where: {
                id
            }
        });

        return deletedCar;
    }
    
}

module.exports = CarsRepository;
